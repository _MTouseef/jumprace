using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wings : MonoBehaviour
{
    // Start is called before the first frame update
   public bool invert;
    void Start()
    {
        if (!invert)
            In();

        if (invert)
            outt();
    }


    void In()
    {
        gameObject.LeanRotateY(transform.eulerAngles.y + 50, 1f).setOnComplete(delegate ()
         {
             outt();
         });
    }

    void outt()
    {
        gameObject.LeanRotateY(transform.eulerAngles.y - 50, 1f).setOnComplete(delegate ()
                 {
                     In();
                 });
    }
}