using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trampolin : MonoBehaviour
{
    // Start is called before the first frame update
    public SpriteRenderer renderer;
    Vector3 scale;
    public int trampolineIndex;
    float time;
    public bool canmove;
    public GameObject linepoint;
    public SkinnedMeshRenderer skinnedMeshRenderer;
    public Mesh mesh;
    void Start()
    {
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        mesh = GetComponent<SkinnedMeshRenderer>().sharedMesh;

        scale = renderer.transform.localScale;
        string name = transform.parent.name;
        int.TryParse(name, out trampolineIndex);
        time = Random.Range(0.8f, 2);
        if (!canmove)
            return;
        int tukka = Random.Range(0, 3);
        if (tukka == 0)
            right();
        else if (tukka == 1)
            left();



    }
    public void rippleUp()
    {
        LeanTween.value(gameObject, 0, 170, 0.1f).setOnUpdate(delegate (float val)
                   {
                       skinnedMeshRenderer.SetBlendShapeWeight(0, val);
                   }).setOnComplete(rippledown);
    }

    void smallup()
    {
        LeanTween.value(gameObject, 0, 50, 0.09f).setOnUpdate(delegate (float val)
                   {
                       skinnedMeshRenderer.SetBlendShapeWeight(0, val);
                   }).setOnComplete(smalldown);

    }
    void smalldown()
    {
        LeanTween.value(gameObject, 50, 0, 0.1f).setOnUpdate(delegate (float val)
                   {
                       skinnedMeshRenderer.SetBlendShapeWeight(0, val);
                   });

    }
    public void rippledown()
    {
        LeanTween.value(gameObject, 170, 0, 0.1f).setOnUpdate(delegate (float val)
                   {
                       skinnedMeshRenderer.SetBlendShapeWeight(0, val);
                   }).setOnComplete(smallup);

    }

    public void fadeout()
    {

        renderer.transform.localScale = scale;
        Color color = renderer.color;
        renderer.color = new Color(color.r, color.g, color.b, 1);
        LeanTween.value(gameObject, 1, 0, 0.7f).setOnUpdate(delegate (float val)
           {

              // Color color = renderer.color;
               renderer.color = new Color(color.r, color.g, color.b, val);
           }).setIgnoreTimeScale(true);
        renderer.transform.LeanScale(renderer.transform.localScale * 2f, 0.8f).setIgnoreTimeScale(true);
    }
    void left()
    {
        transform.parent.LeanMoveX(transform.parent.position.x + 0.5f, time).setOnComplete(delegate ()
        {
            right();
        });
        linepoint.LeanMoveX(linepoint.transform.position.x + 0.5f, time);
    }

    void right()
    {
        transform.parent.LeanMoveX(transform.parent.position.x - 0.5f, time).setOnComplete(delegate ()
        {
            left();
        });
        linepoint.LeanMoveX(linepoint.transform.position.x - 0.5f, time);

    }

}
