using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class EventManager : MonoBehaviour
{
    public static EventManager _instance;

    private void Awake()
    {
        _instance = this;
    }
    public UnityEvent gameover;
    public UnityEvent levelfailed;
}
