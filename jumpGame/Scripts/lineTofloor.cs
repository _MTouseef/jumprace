using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lineTofloor : MonoBehaviour
{
    public LayerMask groundlayer;

    public Transform Playerpoint, Groundpoint;
    public Material linematerial;
    public enum Coulor { none, red, green };
    public Coulor coulor;
    public Color green_col, red_col;
    PLayerV2 playerscript;
    Vector3 offset;
    private void Start()
    {
        playerscript = GetComponent<PLayerV2>();
        offset = Playerpoint.position - transform.position;
        EventManager._instance.levelfailed.AddListener(delegate
        {
            Playerpoint.gameObject.SetActive(false);
        });

        EventManager._instance.gameover.AddListener(delegate
       {
           Playerpoint.gameObject.SetActive(false);
       });
    }
    private void Update()
    {
        Playerpoint.position = transform.position + offset;
        Ray ray = new Ray(transform.position, -transform.up * 100);
        Debug.DrawRay(transform.position, -transform.up * 50, Color.red, 0.01f);
        RaycastHit hitinfo;
        if (Physics.Raycast(transform.position, -transform.up, out hitinfo, 50, groundlayer))
        {
            if (hitinfo.collider)
            {

                Groundpoint.transform.position = hitinfo.point;
                // if (!playerscript._Trampolin)
                // {
                //     trampolin _trampoline = hitinfo.collider.GetComponent<trampolin>();
                //     if (_trampoline)
                //         if (playerscript._Trampolin != _trampoline)
                //             playerscript._Trampolin = hitinfo.collider.GetComponent<trampolin>();
                // }
                red();
            }
            else
            {
                Groundpoint.position += new Vector3(0, -50, 0);
                green();
            }

        }

        else
        {
            Groundpoint.position += new Vector3(0, -50, 0);
            green();
        }

    }
    void green()
    {
        if (coulor == Coulor.green)
            return;
        LeanTween.value(gameObject, red_col, green_col, 0f).setOnUpdate(delegate (Color color)
            {
                linematerial.SetColor("_TintColor", color);
                coulor = Coulor.green;

            });
    }
    void red()
    {
        if (coulor == Coulor.red)
            return;
        LeanTween.value(gameObject, green_col, red_col, 0f).setOnUpdate(delegate (Color color)
           {
               linematerial.SetColor("_TintColor", color);
               coulor = Coulor.red;
           });
    }
}
