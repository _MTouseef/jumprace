using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Guimanager : MonoBehaviour
{
    public static Guimanager _instance;
    public rectransformss rectransformsss;
    public gameoverpanel _gameoverpanel;
    public levelfinish _levelFinish;
    public string[] perfectmessages;
    private void Awake()
    {
        _instance = this;
       EventManager._instance.levelfailed.AddListener(delegate
        {
            Invoke("showgamover", 0.5f);
        });

        EventManager._instance.gameover.AddListener(delegate
        {
            Invoke("showlevelcomplete", 0.5f);
        });
    }
    private void Start()
    {
        rectransformsss.transition.gameObject.SetActive(true);
        LeanTween.value(gameObject, 1, 0, 0.3f).setOnUpdate(delegate (float va)
           {
               rectransformsss.transition.alpha = va;

           }).setOnComplete(delegate ()
           {
               rectransformsss.transition.gameObject.SetActive(false);
           });
    }
    public void apply_progress(float val)
    {
        int total = GameManager._instance.trampolins.Length;
        float value = val / total;
        Debug.Log(value);
        rectransformsss.levelprogressimg.LeanScaleX(value, 0.2f);
    }
    public void prefect()
    {
        rectransformsss.perfect.LeanScale(Vector3.one, 0.3f).setEaseSpring();
        rectransformsss.perfect.GetComponent<Text>().text = perfectmessages[Random.Range(0, perfectmessages.Length - 1)];
        Invoke("hideperfect", 0.8f);
    }
    void hideperfect()
    {
        rectransformsss.perfect.LeanScale(Vector3.zero, 0.3f).setEaseSpring();
    }
    void showgamover()
    {

        rectransformsss.touchfield.SetActive(false);
        Time.timeScale = 0;
        _gameoverpanel.paneel.gameObject.SetActive(true);

        _gameoverpanel.paneel.LeanScale(Vector3.one, 0.2f);
        LeanTween.value(0, 1, 0.1f).setOnUpdate(delegate (float val)
          {
              foreach (CanvasGroup canvas in _gameoverpanel.canvasGroups)
              {
                  canvas.alpha = val;
                  canvas.GetComponent<RectTransform>().LeanScale(Vector3.one, 0.28f).setEaseSpring().setIgnoreTimeScale(true);
              }
          }).setIgnoreTimeScale(true).setOnComplete(delegate ()
          {
              grow(_gameoverpanel.retryBTn);
          });
    }

    void showlevelcomplete()
    {

        rectransformsss.touchfield.SetActive(false);
        _levelFinish.paneel.gameObject.SetActive(true);
        _levelFinish.paneel.LeanScale(Vector3.one, 0.2f);
        LeanTween.value(0, 1, 0.1f).setOnUpdate(delegate (float val)
          {
              foreach (CanvasGroup canvas in _levelFinish.canvasGroups)
              {
                  canvas.alpha = val;
                  canvas.GetComponent<RectTransform>().LeanScale(Vector3.one, 0.28f).setEaseSpring().setIgnoreTimeScale(true);
              }
          }).setIgnoreTimeScale(true).setOnComplete(delegate ()
          {
              grow(_levelFinish.Continuebtn);
          });
    }

    void grow(RectTransform recTr)
    {
        recTr.LeanScale(recTr.localScale * 1.15f, 0.4f).setEaseOutBack().setOnComplete(delegate ()
           {
               shrink(recTr);
           }).setIgnoreTimeScale(true);
    }

    void shrink(RectTransform recTr)
    {
        recTr.LeanScale(recTr.localScale / 1.15f, 0.4f).setEaseOutBack().setOnComplete(delegate ()
            {
                grow(recTr);
            }).setIgnoreTimeScale(true);
    }

    public void restart()
    {
        rectransformsss.transition.gameObject.SetActive(true);
        Time.timeScale = 1;
        LeanTween.value(gameObject, 0, 1, 0.3f).setOnUpdate(delegate (float va)
           {
               rectransformsss.transition.alpha = va;

           }).setOnComplete(delegate () { LevelManager._instance.retry(); });



    }
    public void _Cont()
    {
        rectransformsss.transition.gameObject.SetActive(true);
        Time.timeScale = 1;
        LeanTween.value(gameObject, 0, 1, 0.3f).setOnUpdate(delegate (float va)
           {
               rectransformsss.transition.alpha = va;

           }).setOnComplete(delegate () { LevelManager._instance.loadnxtlvl(gameObject); });


    }


    [System.Serializable]
    public class rectransformss
    {
        public RectTransform levelprogressimg;
        public RectTransform perfect;
        public GameObject touchfield;
        public CanvasGroup transition;
    }

    [System.Serializable]
    public class gameoverpanel
    {
        public RectTransform paneel;
        public CanvasGroup[] canvasGroups;
        public RectTransform retryBTn;
    }

    [System.Serializable]
    public class levelfinish
    {
        public RectTransform paneel;
        public CanvasGroup[] canvasGroups;
        public RectTransform Continuebtn;
    }

}
