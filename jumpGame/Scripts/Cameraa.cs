using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cameraa : MonoBehaviour
{

    public Transform player;
    Vector3 offset;
    public float lerpr;
    public Transform lookat;

    void Update()
    {
        if (!lookat)
            return;
        transform.LookAt(lookat);
        Vector3 direction = (lookat.position - transform.position);
        Vector3 finalmovement =transform.position +  direction - direction.normalized*5;
        transform.position = Vector3.Lerp(transform.position, finalmovement, Time.deltaTime);

    }
}
