using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLayerV2 : MonoBehaviour
{
    public float forwardspeed;
    public FixedTouchField touchField;
    Rigidbody rigidbody;
    public float Jump_value;
    public Transform floordetector;
    bool isGrounded;
    public LayerMask Groundlayer;
    public enum playerstate { jump, idle }
    public playerstate Playerstate;
    public float distance;
    Animator animator;
    public trampolin _Trampolin;
    float initialspeed;
    public ParticleSystem[] starprefab;
    public GameObject line;
    public int trampolinindex;
    Vector3 flooordetector_offset;
    public bool center;
    public GameObject ripple;
    public bool gameover;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        initialspeed = forwardspeed;
        EventManager._instance.gameover.AddListener(delegate
         {
             Camera.main.transform.parent = null;
             gameover = true; animator.SetTrigger("dance");
             Camera.main.GetComponent<Cameraa>().lookat = transform;
             animator.applyRootMotion = true;
             Transform trampoline = GameManager._instance.trampolins[trampolinindex - 1];
             Vector3 direction = trampoline.position - transform.position;
             Quaternion rotation = Quaternion.LookRotation(direction);
             gameObject.LeanRotateY(rotation.eulerAngles.y, 0.5f);
             rigidbody.isKinematic = true;
         });
        flooordetector_offset = floordetector.transform.position - transform.position;
    }

    public void jump(bool rotate = true)
    {
        rigidbody.velocity = Vector3.zero;
        if (!center)
            rigidbody.AddForce(Vector3.up * Jump_value);
        if (center)
        {
            Invoke("slowtime", 0.04f);
            rigidbody.AddForce(Vector3.up * Jump_value * 1.2f);
        }
        Playerstate = playerstate.jump;
        animator.SetTrigger("flip");
        Invoke("jumpfalse", 0.34f);
        if (_Trampolin)
        {
            _Trampolin.rippleUp();
            _Trampolin.fadeout();
            trampolinindex = _Trampolin.trampolineIndex;
            Guimanager._instance.apply_progress(trampolinindex + 1);
            if (rotate)
                rotateto_trampoline();
        }

    }
    void Update()
    {
        if (gameover)
            return;
        if (touchField.TouchDist.magnitude != 0)
            LeanTween.cancel(gameObject);

        floordetector.position = transform.position + flooordetector_offset;
        isGrounded = Physics.CheckSphere(floordetector.position, 0.2f, Groundlayer);
        Quaternion rotation = transform.rotation * Quaternion.Euler(0, touchField.TouchDist.x, 0);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * 18);
        // moveposition = transform.forward * Time.deltaTime * forwardspeed;
        if (isGrounded && Playerstate != playerstate.jump)
        {
            jump();
        }
    }
    private void FixedUpdate()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            rigidbody.MovePosition(transform.position + transform.forward * Time.deltaTime * forwardspeed);
        }
#endif
#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            rigidbody.MovePosition(transform.position + transform.forward * Time.deltaTime * forwardspeed);
        }
#endif
    }

    private void OnTriggerEnter(Collider other)
    {

        string tag = other.tag;
        switch (tag)
        {
            case "point":
                _Trampolin = other.GetComponent<trampolin>();
                break;
            case "center":
                other.GetComponent<center>().slowdown();
                break;
            case "sea":
                Instantiate(ripple, transform.position + (Vector3.up * 0.3f), ripple.transform.rotation);
                Camera.main.transform.parent = null;
                EventManager._instance.levelfailed?.Invoke();
                gameObject.SetActive(false);
                break;
            case "longjump":
                longjump();
                break;
        }
    }
    #region Functions

    void longjump()
    {
        rigidbody.velocity = Vector3.zero;
        rigidbody.AddForce(Vector3.up * Jump_value * 2.5f);
        Playerstate = playerstate.jump;
        forwardspeed = initialspeed;
        animator.SetTrigger("flip");
        Invoke("jumpfalse", 0.34f);
        if (_Trampolin)
        {
            _Trampolin.rippleUp();
            _Trampolin.fadeout();
        }
    }
    void slowtime()
    {
        Guimanager._instance.prefect();
        line.SetActive(true);
        foreach (ParticleSystem star in starprefab)
        {
            star.Play();
        }

        Time.timeScale = 0.2f;
        Time.fixedDeltaTime = 0.2f * 0.02f;
        center = false;
        Invoke("rtime", 0.09f);
        Invoke("lineoff", 1f);
    }
    void lineoff() => line.SetActive(false);
    void rtime()
    {
        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02f;
    }
    void jumpfalse() => Playerstate = playerstate.idle;
    void rotateto_trampoline()
    {
        Transform trampoline = GameManager._instance.trampolins[trampolinindex + 1];
        Vector3 direction = trampoline.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(direction);
        gameObject.LeanRotateY(rotation.eulerAngles.y, 0.3f);
    }
    #endregion

}
