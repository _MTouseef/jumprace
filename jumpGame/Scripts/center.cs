using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class center : MonoBehaviour
{
    public bool slowddown;
    PLayerV2 pLayer;
    public void slowdown()
    {
        if (!slowddown)
        {
            slowddown = true;
            pLayer = GameObject.FindObjectOfType<PLayerV2>();
            pLayer.center = true;
        }
    }
    void restore()
    {
        Time.fixedDeltaTime = 0.02f;
        Time.timeScale = 1;

    }
    void jump()
    {
        pLayer.jump();
        pLayer.center = false;
    }

}
