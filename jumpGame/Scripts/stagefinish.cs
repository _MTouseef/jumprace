using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stagefinish : MonoBehaviour
{

    public ParticleSystem[] confettis;
    private void Start()
    {

        EventManager._instance.gameover.AddListener(playconfetti);
    }
    void playconfetti()
    {
        foreach (ParticleSystem confetti in confettis)
        {
            confetti.Play();
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PLayerV2>())
        {
            EventManager._instance.gameover?.Invoke();
        }
    }



}
