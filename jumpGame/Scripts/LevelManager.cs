﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelManager : MonoBehaviour
{
    static public LevelManager _instance;
    [HideInInspector] public int Thislvlindex;
    public LevelAsset Leveldata;
    [SerializeField] bool testlevel;
    [SerializeField] int testlvlindex;
    private void Awake()
    {
        if (!_instance)
            _instance = this;

    }
    private void Start()
    {
        if (!PlayerPrefs.HasKey("lvl"))
        {
            PlayerPrefs.SetInt("lvl", 1);
        }
        if (testlevel) PlayerPrefs.SetInt("lvl", testlvlindex);


        Thislvlindex = PlayerPrefs.GetInt("lvl");
        if (SceneManager.GetActiveScene().name == "Home")
        {
            loadscene();
        }



    }

    public void loadnxtlvl(GameObject obj)
    {
       Debug.Log("load next level called by  =" + obj.name);
        if (PlayerPrefs.GetInt("lvl") <= Leveldata.TotalLevels - 1)
        {
            int lvl = PlayerPrefs.GetInt("lvl");
            PlayerPrefs.SetInt("lvl", lvl + 1);
        }

        else { PlayerPrefs.SetInt("lvl", 1); }

        loadscene();


    }
    public void loadscene() => SceneManager.LoadSceneAsync(PlayerPrefs.GetInt("lvl"));

    public void retry()
    {
        // UnityEngine.SceneManagement.SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        loadscene();
    }





}


