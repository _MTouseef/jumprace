using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fractured : MonoBehaviour
{

    [SerializeField] Rigidbody[] rigidbodies;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PLayerV2>())
        {
            fracture();
            // other.GetComponent<PLayerV2>().trampolinindex++;
            other.GetComponent<PLayerV2>().jump(false);
        }
    }


    void fracture()
    {
        foreach (Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = false;
            rb.transform.SetParent(null);
            if (rb.GetComponent<pieces>())
            {
                rb.GetComponent<pieces>().scale();
            }
        }
        gameObject.SetActive(false);
        //        offjump.SetActive(false);
        //neechywalaobject.SetActive(false);
    }//fracture


}//class
