using UnityEngine;

[CreateAssetMenu(fileName = "LevelAsset", menuName = "Capture The Flag/LevelAsset", order = 0)]
public class LevelAsset : ScriptableObject
{
public int TotalLevels;

}